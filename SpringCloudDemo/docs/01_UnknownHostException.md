# 一、异常现象

## 1.1 异常堆栈

```java
path [] threw exception [Request processing failed; nested exception is org.springframework.web.client.ResourceAccessException: I/O error on GET request for "http://nacos-provider/hello/geek": nacos-provider; nested exception is java.net.UnknownHostException: nacos-provider] with root cause

java.net.UnknownHostException: nacos-provider
	at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:184) ~[na:1.8.0_121]
	at java.net.PlainSocketImpl.connect(PlainSocketImpl.java:172) ~[na:1.8.0_121]
	at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392) ~[na:1.8.0_121]
	at java.net.Socket.connect(Socket.java:589) ~[na:1.8.0_121]
	at java.net.Socket.connect(Socket.java:538) ~[na:1.8.0_121]
	at sun.net.NetworkClient.doConnect(NetworkClient.java:180) ~[na:1.8.0_121]
	at sun.net.www.http.HttpClient.openServer(HttpClient.java:432) ~[na:1.8.0_121]
	at sun.net.www.http.HttpClient.openServer(HttpClient.java:527) ~[na:1.8.0_121]
	at sun.net.www.http.HttpClient.<init>(HttpClient.java:211) ~[na:1.8.0_121]
	at sun.net.www.http.HttpClient.New(HttpClient.java:308) ~[na:1.8.0_121]
	at sun.net.www.http.HttpClient.New(HttpClient.java:326) ~[na:1.8.0_121]
	at sun.net.www.protocol.http.HttpURLConnection.getNewHttpClient(HttpURLConnection.java:1202) ~[na:1.8.0_121]
	at sun.net.www.protocol.http.HttpURLConnection.plainConnect0(HttpURLConnection.java:1138) ~[na:1.8.0_121]
	at sun.net.www.protocol.http.HttpURLConnection.plainConnect(HttpURLConnection.java:1032) ~[na:1.8.0_121]
	at sun.net.www.protocol.http.HttpURLConnection.connect(HttpURLConnection.java:966) ~[na:1.8.0_121]
	at org.springframework.http.client.SimpleBufferingClientHttpRequest.executeInternal(SimpleBufferingClientHttpRequest.java:76) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.http.client.AbstractBufferingClientHttpRequest.executeInternal(AbstractBufferingClientHttpRequest.java:48) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.http.client.AbstractClientHttpRequest.execute(AbstractClientHttpRequest.java:66) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.client.RestTemplate.doExecute(RestTemplate.java:776) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.client.RestTemplate.execute(RestTemplate.java:711) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.client.RestTemplate.getForObject(RestTemplate.java:334) ~[spring-web-5.3.22.jar:5.3.22]
	at com.springcloud.ms.controller.NacosConsumerController.echo(NacosConsumerController.java:31) ~[classes/:na]
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) ~[na:1.8.0_121]
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62) ~[na:1.8.0_121]
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43) ~[na:1.8.0_121]
	at java.lang.reflect.Method.invoke(Method.java:498) ~[na:1.8.0_121]
	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:205) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:150) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117) ~[spring-webmvc-5.3.22.jar:5.3.22]
	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895) ~[spring-webmvc-5.3.22.jar:5.3.22]
```



## 1.2 导致异常出现的配置

主要将 SpringCloud 的版本调成了 2021.0.4 版本，

然后将 spring-cloud-starter-alibaba-nacos-discovery 调成了 2021.0.4.0 版本。

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <packaging>pom</packaging>
  <modules>
    <module>sc01-eureka</module>
    <module>sc02-nacos</module>
    <module>sc03-cluster</module>
  </modules>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.6.11</version>
  </parent>

  <groupId>com.springcloud.ms</groupId>
  <artifactId>parent</artifactId>
  <version>1.0-SNAPSHOT</version>

  <name>parent</name>
  <url>http://www.example.com</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>

    <spring-cloud.version>2021.0.4</spring-cloud.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>${spring-cloud.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        <version>2021.0.4.0</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

</project>

```



# 二、异常分析



## 2.1 初步分析

spring-cloud-dependencies = 2021.0.6、spring-cloud-starter-alibaba-nacos-discovery = 2.2.8.RELEASE 没问题，

但是配置成

spring-cloud-dependencies = 2021.0.4、spring-cloud-starter-alibaba-nacos-discovery = 2021.0.4.0 却有问题，

而且异常的原因发生在

```java
 restTemplate.getForObject("http://nacos-provider/hello/"+id, String.class); 
```

这行代码上，出现的异常是 UnknownHostException 异常。



从异常的明面上看，是没有给 nacos-provider 域名配置 HOSTS 映射关系，实际上是没有起到**有效的负载均衡作用**。



## 2.2 验证正常配置的启动问题

### 2.2.1 启动正常的配置

```properties
spring-cloud-dependencies = 2021.0.6
spring-cloud-starter-alibaba-nacos-discovery = 2.2.8.RELEASE
```



### 2.2.2 查看调用堆栈信息

从这个正常调用的堆栈，可以发现 getForObject 方法后面确实调用了 LoadBalancerInterceptor 的方法，说明确实走到了负载均衡相关的类里面去了。

![image-20230725230951409](README-IMAGES/image-20230725230951409.png)





## 2.3验证异常配置的启动问题



### 2.3.1 启动异常的配置

```properties
spring-cloud-dependencies = 2021.0.4
spring-cloud-starter-alibaba-nacos-discovery = 2021.0.4.0
```



### 2.3.2 查看调用堆栈信息

从 RestTemplate 到 AbstractClientHttpRequest 再到 SimpleBufferingClientHttpRequest，压根就没发现所谓的 LoadBalancerInterceptor 这个方法，也没有发现所谓的 InterceptingClientHttpRequest 类。

因此这里就更加肯定的说明了一点，没有走到负载均衡相关的逻辑里面去。



同时检查 nacos 的控制台，却发现提供方、消费方都已经注册到 nacos 了，那就说明不是 nacos 服务端的问题了，更加证实了是因为负载出现了问题。

```
	at org.springframework.http.client.SimpleBufferingClientHttpRequest.executeInternal(SimpleBufferingClientHttpRequest.java:76) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.http.client.AbstractBufferingClientHttpRequest.executeInternal(AbstractBufferingClientHttpRequest.java:48) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.http.client.AbstractClientHttpRequest.execute(AbstractClientHttpRequest.java:66) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.client.RestTemplate.doExecute(RestTemplate.java:776) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.client.RestTemplate.execute(RestTemplate.java:711) ~[spring-web-5.3.22.jar:5.3.22]
	at org.springframework.web.client.RestTemplate.getForObject(RestTemplate.java:334) ~[spring-web-5.3.22.jar:5.3.22]
```



## 2.4 基于报错工程如何解决



现在的问题就转为了，在报错的工程中，如何使得 RestTemplate.getForObject 这行代码进入负载均衡逻辑。



### 2.4.1 尝试看看 ribbon 相关的包

发现 ribbon 相关的包都有，版本对不对暂且不说。

![image-20230725232944434](README-IMAGES/image-20230725232944434.png)





### 2.4.2 尝试看看 loadbalance 相关的包

注意这个 loadbalance 单词不用输入完整，只需要输入一部分看看到底是否有相关的包。

![image-20230725233149915](README-IMAGES/image-20230725233149915.png)





### 2.4.3 分析搜索结果

通过检索发现进行一些因素的对比，ribbon 有 starter 组件，但是 loadbalancer 没有 starter 组件，当然这只是推测，但是可以尝试看看是否有这样的 loadbalancer 组件。



结果在 dependency 中的 artifactId 输入 spring-cloud-starter-load 的时候，居然有智能提示，那么就索性直接引入这个 pom 坐标看看。

```xml
      <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        <version>3.1.0</version>
      </dependency>
```



### 2.4.4 引入 loadbalancer 的 starter 组件

```xml
pom.xml


<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <packaging>pom</packaging>
  <modules>
    <module>sc01-eureka</module>
    <module>sc02-nacos</module>
    <module>sc03-cluster</module>
  </modules>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.6.11</version>
  </parent>

  <groupId>com.springcloud.ms</groupId>
  <artifactId>parent</artifactId>
  <version>1.0-SNAPSHOT</version>

  <name>parent</name>
  <url>http://www.example.com</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>

    <spring-cloud.version>2021.0.4</spring-cloud.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>${spring-cloud.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        <version>2021.0.4.0</version>
      </dependency>
      <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        <version>3.1.0</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

</project>
```



```xml
sc02-nacos\nacos-consumer\pom.xml


<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>sc02-nacos</artifactId>
        <groupId>com.springcloud.ms</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>nacos-consumer</artifactId>

    <name>nacos-consumer</name>
    <url>http://www.example.com</url>

    <properties>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        </dependency>

    </dependencies>

</project>

```





### 2.4.5 启动工程

运行 src\main\java\com\springcloud\ms\NacosProviderApplication.java，

再运行 src\main\java\com\springcloud\ms\NacosConsumerApplication.java



然后在浏览器请求  http://localhost:9011/helloworld/geek  地址，最后正常看到了结果，由此说明确实是缺少包造成的。



## 2.5 其他解决途径

你可能会想，刚刚这一路排查，似乎推出了缺少包，但是不知道缺具体什么包，这就难道我们了。

但是你要想一个问题，我们身处在网络之中，这些平常的问题，我们可以通过一些关键字检索得到答案。



### 2.5.1 百度检索

比如我们现在正在使用 nacos 进行远程访问调用，然而出现了 UnknownHostException 异常，那么就可以试着这样来检索：

![image-20230725234851648](README-IMAGES/image-20230725234851648.png)



### 2.5.2 分析百度检索结果

然后随便点进去几个帖子，你就会发现大家的智慧结晶，会尝试着告诉你去引用  spring-cloud-starter-loadbalancer 这个包，然后就正常了。



然后你只有遇到的报错，你才会了解真实原因，百度有这么一句话描述着：SpringCloud2020.0.1.0之后版本不使用netflix了。



因此有时候，你无法事先知道所有的事情，你只有遇到问题分析后，一步一步走下去，你才会发现之前学过的原来是在重点描述这些细节，但是这些细节没有多少人会去认真研读官网的每个细节的，但是你要做的就是去分析，分析问题可能产线的原因，然后结合自己掌握的技能去逐个突破，最终找到问题的本质原因。