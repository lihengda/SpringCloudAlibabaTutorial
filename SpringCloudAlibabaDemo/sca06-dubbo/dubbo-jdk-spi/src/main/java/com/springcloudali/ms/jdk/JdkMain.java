package com.springcloudali.ms.jdk;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 * ^_^
 * @version 0.0.1
 * ^_^
 * @date 2023-05-18
 */
public class JdkMain {

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            ServiceLoader<JDKSPI> loader = ServiceLoader.load(JDKSPI.class);
            Iterator<JDKSPI> it = loader.iterator();
            while (it.hasNext()){
                JDKSPI service = it.next();
                if(service.getClass().getName().contains("SpringSPI")){
                    // 比如把 service 返回出去
                    return;
                }
                System.out.println(service.spi());
            }
            System.out.println();
        }
        System.out.println("加载结束");
    }
}
