package com.springcloudali.ms;

import com.springcloudali.ms.invoke.InvokeService;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 *         ^_^
 * @version 0.0.1
 *         ^_^
 * @date 2023-06-17
 *
 */
@SpringBootApplication
@EnableDubbo
public class DubboCheckConsumerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DubboCheckConsumerApplication.class);
        System.out.println("【【【  DubboCheckConsumerApplication 启动成功  】】】");

        // 首次发起远程调用
        InvokeService service = ctx.getBean(InvokeService.class);
        try {
            System.out.println("消费方调用结果：" + service.invoke("Geek"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 模拟睡眠 20 秒，主要是想等待 dubbo-discovery-provider 提供方启动成功，然后再次尝试调用提供方
        System.out.println("睡眠开始...");
        try {
            Thread.sleep(30 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("睡眠结束...");

        try {
            System.out.println("消费方再次调用结果：" + service.invoke("Geek"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}