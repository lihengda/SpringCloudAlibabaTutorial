//package com.springcloudali.ms.socket;
//
//import java.net.ServerSocket;
//import java.net.Socket;
//
///**
// * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
// *
// * @author hmilyylimh
// * ^_^
// * @version 0.0.1
// * ^_^
// * @date 2023-05-20
// */
//public class SocketTest {
//
//    public static void main(String[] args) {
//        // 消费方，请求方
//        Socket socket = new Socket("127.0.0.1", 6789);
//        socket.getOutputStream().write( 写多少数据，写 100 字节的数据);
//
//
//        // 服务方，接收方
//        ServerSocket serverSocket = new ServerSocket(6789);
//        Socket acceptSocket = serverSocket.accept();
//        acceptSocket.getInputStream().read( 读取多少数据呢？？？)
//
//    }
//}
