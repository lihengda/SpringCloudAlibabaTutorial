package com.springcloudali.ms.custom;

/**
 *
 *
 * @author hmilyylimh
 *         ^_^
 * @version 0.0.1
 *         ^_^
 * @date 2023-05-20
 *
 */
public abstract class CustomInvoker {

    public Object invokeMethod(Object instance, String methodName, Class<?>[] types, Object[] args) throws NoSuchMethodException {
        throw new NoSuchMethodException("Method [" + methodName + "] not found.");
    }
}
