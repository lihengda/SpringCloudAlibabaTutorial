package com.springcloudali.ms.custom;

import com.springcloudali.ms.facde.DemoFacade;

/**
 *
 *
 * @author hmilyylimh
 *         ^_^
 * @version 0.0.1
 *         ^_^
 * @date 2023-05-20
 *
 */
public class $DemoFacadeCustomInvoker extends CustomInvoker {

    @Override
    public Object invokeMethod(Object instance, String mtdName, Class<?>[] types, Object[] args) throws NoSuchMethodException {
        if ("sayHello".equals(mtdName)) {
            return ((DemoFacade) instance).sayHello(String.valueOf(args[0]));
        } else if ("say".equals(mtdName)) {
            return ((DemoFacade) instance).say();
        }

        throw new NoSuchMethodException("Method [" + mtdName + "] not found.");
    }
}