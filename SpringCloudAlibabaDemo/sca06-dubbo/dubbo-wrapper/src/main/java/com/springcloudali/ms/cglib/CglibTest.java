package com.springcloudali.ms.cglib;

import com.springcloudali.ms.demo.DemoFacadeImpl;
import com.springcloudali.ms.facde.DemoFacade;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 * ^_^
 * @version 0.0.1
 * ^_^
 * @date 2023-05-20
 */
public class CglibTest {

    public static void main(String[] args) {
        DemoFacade demoFacade = new DemoFacadeImpl();

        Enhancer enhancer = new Enhancer();
        // 设置代理什么类
        enhancer.setSuperclass(demoFacade.getClass());
        // 设置代理处理器
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                if("sayHello".equals(method.getName())){
                    String result = String.format("Hello %s, I'm in 'sca06-dubbo/dubbo-wrapper/CglibTest' project.", objects[0]);
                    return result;
                }
                // 是不是还是存在找的这个硬编码过程？

                // 或者再次用反射进行调用？
                Class<?> clz = method.getDeclaringClass();
                String methodName = method.getName();
                Class<?>[] parameterTypes = method.getParameterTypes();
                // 找到之后进行进行调用
                return null;
            }
        });
        DemoFacade demoFacadeproxy = (DemoFacade)enhancer.create();
        System.out.println(demoFacadeproxy.sayHello("Geek"));
    }
}