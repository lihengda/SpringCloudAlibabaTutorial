//package com.springcloudali.ms.adaptive;
//
///**
// * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
// *
// * @author hmilyylimh
// * ^_^
// * @version 0.0.1
// * ^_^
// * @date 2023-05-20
// */
//
//import org.apache.dubbo.common.extension.ExtensionLoader;
//
//package org.apache.dubbo.rpc;
//
//public class Protocol$Adaptive implements org.apache.dubbo.rpc.Protocol {
//    public void destroy() {
//        throw new UnsupportedOperationException("The method public abstract void org.apache.dubbo.rpc.Protocol" +
//                ".destroy() of interface org.apache.dubbo.rpc.Protocol is not adaptive method!");
//    }
//
//    public int getDefaultPort() {
//        throw new UnsupportedOperationException("The method public abstract int org.apache.dubbo.rpc.Protocol" +
//                ".getDefaultPort() of interface org.apache.dubbo.rpc.Protocol is not adaptive method!");
//    }
//
//    public org.apache.dubbo.rpc.Exporter export(org.apache.dubbo.rpc.Invoker arg0) throws org.apache.dubbo.rpc.RpcException {
//        if (arg0 == null) throw new IllegalArgumentException("org.apache.dubbo.rpc.Invoker argument == null");
//        if (arg0.getUrl() == null)
//            throw new IllegalArgumentException("org.apache.dubbo.rpc.Invoker argument getUrl() == null");
//        org.apache.dubbo.common.URL url = arg0.getUrl();
//        String extName = (url.getProtocol() == null ? "dubbo" : url.getProtocol());
//        if (extName == null)
//            throw new IllegalStateException("Failed to get extension (org.apache.dubbo.rpc.Protocol) name from url (" + url.toString() + ") use keys([protocol])");
//        org.apache.dubbo.rpc.Protocol extension =
//                (org.apache.dubbo.rpc.Protocol) ExtensionLoader.getExtensionLoader(org.apache.dubbo.rpc.Protocol.class).getExtension(extName);
//        return extension.export(arg0);
//    }
//
//    public org.apache.dubbo.rpc.Invoker refer(java.lang.Class arg0, org.apache.dubbo.common.URL arg1) throws org.apache.dubbo.rpc.RpcException {
//        if (arg1 == null) throw new IllegalArgumentException("url == null");
//        org.apache.dubbo.common.URL url = arg1;
//        String extName = (url.getProtocol() == null ? "dubbo" : url.getProtocol());
//        if (extName == null)
//            throw new IllegalStateException("Failed to get extension (org.apache.dubbo.rpc.Protocol) name from url (" + url.toString() + ") use keys([protocol])");
//        org.apache.dubbo.rpc.Protocol extension =
//                (org.apache.dubbo.rpc.Protocol) ExtensionLoader.getExtensionLoader(org.apache.dubbo.rpc.Protocol.class).getExtension(extName);
//        return extension.refer(arg0, arg1);
//    }
//
//    public java.util.List getServers() {
//        throw new UnsupportedOperationException("The method public default java.util.List org.apache.dubbo.rpc" +
//                ".Protocol.getServers() of interface org.apache.dubbo.rpc.Protocol is not adaptive method!");
//    }
//}