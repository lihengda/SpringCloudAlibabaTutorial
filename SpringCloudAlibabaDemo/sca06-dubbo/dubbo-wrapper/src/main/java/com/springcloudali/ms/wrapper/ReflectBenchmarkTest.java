package com.springcloudali.ms.wrapper;

import org.apache.dubbo.common.bytecode.Wrapper;

import java.lang.reflect.Method;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 *         ^_^
 * @version 0.0.1
 *         ^_^
 * @date 2023-06-17
 *
 */
public class ReflectBenchmarkTest {
    public static void main(String[] args) {
        int size = 1000000;
        // 循环正常创建类调用某个方法，并打印耗时的时间
        long start = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            normalInvoke();
        }
        long end = System.currentTimeMillis();
        System.out.println("正常调用耗时为：" + (end - start) + " 毫秒");
        
        // 循环反射创建类调用某个方法，并打印耗时的时间
        start = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            reflectInvoke();
        }
        end = System.currentTimeMillis();
        System.out.println("反射调用耗时为：" + (end - start) + " 毫秒");

        // 循环进行Wrapper调用，并打印耗时的时间
        Log4jSPI log4jSPI = new Log4jSPI();
        final Wrapper wrapper = Wrapper.getWrapper(log4jSPI.getClass());
        start = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            wrapperInvoke(wrapper, log4jSPI);
        }
        end = System.currentTimeMillis();
        System.out.println("Wrapper调用耗时为：" + (end - start) + " 毫秒");
    }
    // 正常创建对象，并调用对象的方法
    public static void normalInvoke(){
        Log4jSPI customSpi = new Log4jSPI();
        customSpi.spi();
    }
    // 反射创建对象，并反射调用对象的方法
    public static void reflectInvoke(){
        try {
            Class<?> clz = Class.forName("com.springcloudali.ms.wrapper.Log4jSPI");
            Object o = clz.newInstance();
            Method method = clz.getDeclaredMethod("spi");
            method.invoke(o);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    // 反射创建对象，并反射调用对象的方法
    public static void wrapperInvoke(Wrapper wrapper, Object reqObj){
        try {
            // 使用生成的 wrapper 代理类调用通用的 invokeMethod 方法获取结果
            wrapper.invokeMethod(
                    reqObj,
                    "spi",
                    new Class[]{},
                    new Object[]{}
            );
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}