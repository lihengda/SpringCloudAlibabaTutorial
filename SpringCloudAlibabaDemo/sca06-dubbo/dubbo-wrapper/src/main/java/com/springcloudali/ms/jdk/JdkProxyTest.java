package com.springcloudali.ms.jdk;

import com.springcloudali.ms.facde.DemoFacade;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 * ^_^
 * @version 0.0.1
 * ^_^
 * @date 2023-05-20
 */
public class JdkProxyTest {

    public static void main(String[] args) {

        DemoFacade demoFacadeProxy = (DemoFacade)Proxy.newProxyInstance(
                JdkProxyTest.class.getClassLoader(),
                new Class[]{DemoFacade.class},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if("sayHello".equals(method.getName())){
                            String result = String.format("Hello %s, I'm in 'sca06-dubbo/dubbo-wrapper/JdkProxyTest' project.", args[0]);
                            return result;
                        }
                        // 是不是还是存在找的这个硬编码过程？

                        // 或者再次用反射进行调用？
                        Class<?> clz = method.getDeclaringClass();
                        String methodName = method.getName();
                        Class<?>[] parameterTypes = method.getParameterTypes();
                        // 找到之后进行进行调用
                        return null;
                    }
                }
        );

        System.out.println(demoFacadeProxy.sayHello("Geek"));
    }
}
