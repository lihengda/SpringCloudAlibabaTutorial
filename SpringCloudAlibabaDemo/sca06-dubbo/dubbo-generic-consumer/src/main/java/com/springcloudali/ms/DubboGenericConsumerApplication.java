package com.springcloudali.ms;

import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 *         ^_^
 * @version 0.0.1
 *         ^_^
 * @date 2023-06-17
 *
 */
@SpringBootApplication
@EnableDubbo
public class DubboGenericConsumerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DubboGenericConsumerApplication.class);
        System.out.println("【【【  DubboGenericConsumerApplication 启动成功  】】】");

        doInvokeGeneric();
    }

    private static void doInvokeGeneric() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("dubbo-generic-consumer");

        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress("nacos://127.0.0.1:8848");

        ReferenceConfig<GenericService> referConfig = new ReferenceConfig<>();
        referConfig.setInterface("com.springcloudali.ms.facde.DemoService");
        referConfig.setRegistry(registryConfig);
        referConfig.setApplication(applicationConfig);
        referConfig.setGeneric("true");
        referConfig.setTimeout(5000);

        GenericService genericService = referConfig.get();
        for (int i = 1; i <= 6; i++) {
            Object result = genericService.$invoke(
                    "sayHello",
                    new String[]{String.class.getName()},
                    new Object[]{"Geek"}
            );
            System.out.println("第 "+ i +" 次泛化同步调用结果：" + result);
        }
    }
}