package com.springcloudali.ms.controller;

import com.springcloudali.ms.feign.DegradeFeignClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 * ^_^
 * @version 0.0.1
 * ^_^
 * @date 2023-05-09
 */
@Component
public class DegradeFeignClientFallbackFactory implements FallbackFactory<DegradeFeignClientFallbackFactory.DegradeFeignClientFallbackFactoryInner> {

    @Override
    public DegradeFeignClientFallbackFactoryInner create(Throwable cause) {
        System.out.println("错误原因: " + cause.getMessage());

        return new DegradeFeignClientFallbackFactoryInner();
    }

    static class DegradeFeignClientFallbackFactoryInner implements DegradeFeignClient {

        @Override
        public String echo(String id) {
            return "FallbackFactory recv args: id=" + id
                    + ", date=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").format(new Date());
        }
    }

}
