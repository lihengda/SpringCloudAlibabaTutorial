package com.springcloudali.ms.feign;

import com.springcloudali.ms.controller.DegradeFeignClientFallback;
import com.springcloudali.ms.controller.DegradeFeignClientFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 * ^_^
 * @version 0.0.1
 * ^_^
 * @date 2023-05-08
 */
@FeignClient(name = "openfeign-provider",
        // fallback = DegradeFeignClientFallback.class,
        fallbackFactory = DegradeFeignClientFallbackFactory.class
)
public interface DegradeFeignClient {

    @RequestMapping(value = "/hello/{id}", method = RequestMethod.GET)
    public String echo(@PathVariable String id) ;
}