package com.springcloudali.ms.controller;

import com.springcloudali.ms.feign.OpenFeignProviderControllerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * <h1>8位技术专家全程教学实战，全面覆盖Java进阶知识体系。<br/><br/><a href="https://u.geekbang.org/subject/java4th/1001148?source=app_share">极客训练营地址：https://u.geekbang.org/subject/java4th/1001148?source=app_share</a></h1><br/><h1><a href="https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial">极客案例代码地址：https://gitee.com/ylimhhmily/SpringCloudAlibabaTutorial</a></h1><br/><h1><a href="https://time.geekbang.org/column/intro/100312101">Dubbo 源码剖析与实战：https://time.geekbang.org/column/intro/100312101</a></h1>
 *
 * @author hmilyylimh
 * ^_^
 * @version 0.0.1
 * ^_^
 * @date 2023-05-08
 */
@RestController
public class OpenFeignConsumerController {

    @Autowired
    private OpenFeignProviderControllerFeignClient openFeignProviderControllerFeignClient;

    // http://localhost:9044/helloworld/geek
    @RequestMapping(value = "/helloworld/{id}", method = RequestMethod.GET)
    public String echo(@PathVariable String id) {
        // return restTemplate.getForObject("http://openfeign-provider/hello/"+id, String.class);

        return openFeignProviderControllerFeignClient.echo(id);
    }
}
